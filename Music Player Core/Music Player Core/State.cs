﻿namespace Music_Player_Core
{
    /// <summary>
    /// Перечисление "Статус операции".
    /// </summary>
    public enum State
    {
        Empty = 0,
        Author = 1,
        Title = 2,
        Qenre = 3,
        Album = 4,
        Source = 5,
        Wait = 6,
    }
}
