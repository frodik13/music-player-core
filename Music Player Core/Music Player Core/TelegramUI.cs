﻿using Music_Player_Core.Commands;
using Music_Player_Core.DataBase;
using Music_Player_Core.DataBase.Tables;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Music_Player_Core
{
    public class TelegramUI
    {
        #region Поля и свойства

        /// <summary>
        /// Клиент.
        /// </summary>
        private readonly TelegramBotClient client;

        private static readonly Lazy<TelegramUI> telegram = new(() => new TelegramUI());

        private const string tocken = "5338038892:AAHlfXNFXgtsJd-O0AUS0G-wdpZ0-fV5UeI";

        /// <summary>
        /// Список всех песен, которые есть в базе.
        /// </summary>
        private List<Track> tracks;

        /// <summary>
        /// Строка директория для бд и файлов.
        /// </summary>
        public static readonly string directory = Environment.CurrentDirectory;

        private readonly List<BaseCommand> commands;

        private MusicDbContext musicDb;

        #endregion

        #region Методы

        /// <summary>
        /// Инициализация всех переменных и запуск бота.
        /// </summary>
        private void Init()
        {
            
        }

        /// <summary>
        /// Получение обновлений.
        /// </summary>
        /// <param name="botClient">Клиент.</param>
        /// <param name="update">Обновления.</param>
        /// <param name="tocken">Токен.</param>
        /// <returns></returns>
        async Task Update(ITelegramBotClient botClient, Update update, CancellationToken tocken)
        {
            try
            {
                if (update.Type == UpdateType.Message)
                {
                    var message = update.Message;

                    if (message != null)
                    {
                        Console.WriteLine(message.Chat.Username + ": " + message.Text);

                        if (message.Text != null)
                        {
                            foreach (var command in commands)
                            {
                                if (command is UploadCommand && command.State != State.Empty)
                                {
                                    message.Text = $"{CommandHelper.UPLOAD_COMMAND} " + message.Text;
                                }

                                if (command is UpdateCommand && command.State != State.Empty)
                                {
                                    message.Text = $"{CommandHelper.UPDATE_COMMAND} " + message.Text;
                                }

                                if (command.ContainsCommand(message.Text))
                                {
                                    LogHelper.Info($"Пользователь {message.Chat.FirstName} {message.Chat.LastName} вызвал команду {message.Text}");
                                    await command.Execute(botClient, update, musicDb);
                                    if (command is DeleteCommand)
                                        if (musicDb.Tracks != null)
                                            tracks = musicDb.Tracks.ToList();
                                }
                                else
                                {
                                    command.State = State.Empty;
                                }
                            }
                        }

                        if (message.Audio != null)
                        {
                            if (commands[6].State == State.Source)
                            {
                                await commands[6].Execute(botClient, update, musicDb);
                                LogHelper.Info($"Пользователь {message.Chat.FirstName} {message.Chat.LastName} отправил файл песни {message.Audio.FileName}");
                                if(musicDb.Tracks != null)
                                    tracks = musicDb.Tracks.ToList();
                            }
                        }
                    }
                }
                else if (update.Type == UpdateType.CallbackQuery)
                {
                    var callback = update.CallbackQuery;
                    if (callback != null)
                    {
                        foreach (var comm in commands)
                        {
                            if (comm.ContainsStateNotEmpty())
                            {
                                if(callback.Message != null)
                                    LogHelper.Info($"Пользователь {callback.Message.Chat.FirstName} {callback.Message.Chat.LastName} выбрал callback {callback.Data}");
                                await comm.Execute(botClient, update, musicDb);
                            }
                            else
                            {
                                comm.State = State.Empty;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (update.Message != null)
                    await botClient.SendTextMessageAsync(update.Message.Chat.Id, "Произошла ошибка в работе, попробуйте снова.");
                if (update.CallbackQuery?.Message != null)
                    await botClient.SendTextMessageAsync(update.CallbackQuery.Message.Chat.Id, "Произошла ошибка в работе, попробуйте снова.");
                Console.WriteLine(ex.Message);
                LogHelper.Error($"Произошла ошибка в работе бота!\t{ex.Message}");
            }

        }

        /// <summary>
        /// Ошибки.
        /// </summary>
        /// <param name="client">Клиент.</param>
        /// <param name="ex">Ошибка.</param>
        /// <param name="tocken">Токен</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        private Task Error(ITelegramBotClient client, Exception ex, CancellationToken tocken)
        {
            Console.WriteLine(ex.Message);
            LogHelper.Error($"Произошла ошибка в работе бота!\t{ex.Message}");
            client.CloseAsync();
            musicDb.Dispose();
            throw ex;
        }

        public static TelegramUI GetInstanse()
        {
            return telegram.Value;
        }

        #endregion

        #region Конструктор

        /// <summary>
        /// Конструктор класса.
        /// </summary>
        private TelegramUI()
        {
            client = new TelegramBotClient(tocken);
            LogHelper.Info("Запуск бота");
            musicDb = new MusicDbContext();
            tracks = new List<Track>();
            if (musicDb.Tracks != null)
                tracks = musicDb.Tracks.ToList();
            commands = new List<BaseCommand>
            {
                new StartCommand(),
                new AllTrackCommand(),
                new SortCommand(),
                new ScanCommand(),
                new PlayCommand(),
                new DeleteCommand(),
                new UploadCommand(),
                new UpdateCommand()
            };
            client.StartReceiving(Update, Error);
        }

        #endregion
    }
}
