﻿using Microsoft.EntityFrameworkCore;
using Music_Player_Core.DataBase;
using Music_Player_Core.DataBase.Tables;

namespace Music_Player_Core
{
    internal class Program
    {
        static void Main(string[] args)
        {
            TelegramUI.GetInstanse();
            Console.ReadLine();
        }
    }
}