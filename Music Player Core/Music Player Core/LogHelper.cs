﻿using NLog;

namespace Music_Player_Core
{
    public static class LogHelper
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public static void Info(string text)
        {
            logger.Info(text);
        }

        public static void Error(string text)
        {
            logger.Error(text);
        }
    }
}
