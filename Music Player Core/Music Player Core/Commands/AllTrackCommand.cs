﻿using Music_Player_Core.DataBase;
using Music_Player_Core.DataBase.Tables;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace Music_Player_Core.Commands
{
    /// <summary>
    /// Команда "Список всех песен".
    /// </summary>
    public class AllTrackCommand : BaseCommand
    {
        #region Поля и свойства

        public override string Name { get; }

        /// <summary>
        /// Список списков песен.
        /// </summary>
        private List<List<Track>> list;

        /// <summary>
        /// Количество листов.
        /// </summary>
        private int amountList;

        /// <summary>
        /// Текущий лист.
        /// </summary>
        private int currentList;

        /// <summary>
        /// ИД редактируемого сообщения.
        /// </summary>
        private int messageId;

        #endregion

        #region Override methods

        public async override Task Execute(ITelegramBotClient client, Update update, MusicDbContext musicDb)
        {
            var message = update.Message;
            var callback = update.CallbackQuery;
            if (message != null)
            {
                var tracks = musicDb?.Tracks?.ToList();
                if (tracks != null)
                    if (tracks.Count > 10)
                    {
                        amountList = (int)Math.Ceiling((decimal)tracks.Count / 10);
                        currentList = 1;
                        list = TrackList(tracks);

                        State = State.Wait;
                        var result = client.SendTextMessageAsync(message.Chat.Id, string.Join("\n", list[currentList - 1]), replyMarkup: InlineKeyboardBackNext(currentList, amountList));
                        messageId = result.Result.MessageId;
                    }
                    else
                        await client.SendTextMessageAsync(message.Chat.Id, string.Join("\n", tracks));
                else
                    await client.SendTextMessageAsync(message.Chat.Id, "Пока еще нету треков.");
                return;
            }
            else if (callback != null)
            {
                if (callback.Message != null)
                {
                    if (callback.Data == "Next")
                    {
                        if (currentList < amountList)
                        {
                            currentList++;
                            await client.EditMessageTextAsync(callback.Message.Chat.Id, messageId, string.Join("\n", list[currentList - 1]), replyMarkup: InlineKeyboardBackNext(currentList, amountList));
                            return;
                        }
                    }
                    else if (callback.Data == "Back")
                    {
                        if (currentList > 1)
                        {
                            currentList--;
                            await client.EditMessageTextAsync(callback.Message.Chat.Id, messageId, string.Join("\n", list[currentList - 1]), replyMarkup: InlineKeyboardBackNext(currentList, amountList));
                            return;
                        }
                    }
                }
            }
        }

        #endregion

        #region Конструктор

        /// <summary>
        /// Конструктор класса.
        /// </summary>
        public AllTrackCommand() : base()
        {
            Name = CommandHelper.ALL_TRACK_COMMAND;
            list = new List<List<Track>>();
        }

        #endregion
    }
}
