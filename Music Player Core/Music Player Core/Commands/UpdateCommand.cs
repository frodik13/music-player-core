﻿using Music_Player_Core.DataBase;
using Music_Player_Core.DataBase.Tables;
using System.Threading.Channels;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace Music_Player_Core.Commands
{
    public class UpdateCommand : BaseCommand
    {
        public override string Name { get; }

        private readonly int lengthCommand = CommandHelper.UPDATE_COMMAND.Length;

        private List<Track> trackUpdate;

        private readonly List<string> values;

        public override async Task Execute(ITelegramBotClient client, Update update, MusicDbContext musicDb)
        {
            var message = update.Message;
            var callback = update.CallbackQuery;
            var keyboard = new InlineKeyboardMarkup(new InlineKeyboardButton(CommandHelper.CANCEL_COMMAND) { CallbackData = CommandHelper.CANCEL_COMMAND });
            var tracks = musicDb?.Tracks?.ToList();

            switch (State)
            {
                case State.Empty:
                    if(message != null)
                        await SendMessage(values, "Что вы хотите изменить?", client, message.Chat.Id);
                    break;
                case State.Wait:
                    if (callback != null)
                    {
                        if (tracks != null && tracks.Any())
                        {
                            if (callback.Data == values[0])
                            { 
                                var valuesAuthor = tracks.Select(x => x.Author).Distinct().ToList();
                                await Selection(client, callback, valuesAuthor, "Выберите автора", State.Author);
                            }
                            else if (callback.Data == values[1])
                            {
                                var valuesAlbum = tracks.Select(x => x.Album).Distinct().ToList();
                                await Selection(client, callback, valuesAlbum, "Выберите альбом", State.Album);
                            }
                            else if (callback.Data == values[2])
                            {
                                var valuesQenre = tracks.Select(x => x.Qenre).Distinct().ToList();
                                await Selection(client, callback, valuesQenre, "Выберите жанр", State.Qenre);
                            }
                            else if (callback.Data == values[3])
                            {
                                var valuesTitle = tracks.Select(x => x.Title).Distinct().ToList();
                                await Selection(client, callback, valuesTitle, "Выберите песню", State.Title);
                            }
                        }
                    }
                    break;
                case State.Author:
                    if (tracks != null && tracks.Any())
                    {

                        if (callback != null && callback.Data != CommandHelper.CANCEL_COMMAND)
                        {
                            foreach (var track in tracks)
                                if (track.Author == callback.Data)
                                    trackUpdate.Add(track);
                            if (callback.Message != null)
                                await client.SendTextMessageAsync(callback.Message.Chat.Id, "Введите новое имя исполнителя", replyMarkup: keyboard);
                        }
                        else if(callback != null && callback.Data == CommandHelper.CANCEL_COMMAND)
                        {
                            ResetState();
                            if (callback.Message != null)
                                await client.SendTextMessageAsync(callback.Message.Chat.Id, "Отмена операции");
                        }
                        if (message?.Text != null)
                        {
                            var author = ReplaceText(message.Text, lengthCommand);
                            foreach (var track in trackUpdate)
                            {
                                if (musicDb?.Tracks != null && musicDb.Tracks.Any())
                                {
                                    var t = musicDb.Tracks.FirstOrDefault(x => x.Author == track.Author);
                                    if (t != null)
                                    {
                                        t.Author = author;
                                        musicDb.Tracks.Update(t);
                                        musicDb.SaveChanges();
                                    }
                                }
                            }
                            await client.SendTextMessageAsync(message.Chat.Id, "Название исполнителя успешно изменено.");
                            trackUpdate.Clear();
                            ResetState();
                        }
                    }
                    break;
                case State.Album:
                    if (tracks != null && tracks.Any())
                    {

                        if (callback != null && callback.Data != CommandHelper.CANCEL_COMMAND)
                        {
                            foreach (var track in tracks)
                                if (track.Album == callback.Data)
                                    trackUpdate.Add(track);
                            if (callback.Message != null)
                                await client.SendTextMessageAsync(callback.Message.Chat.Id, "Введите новое название альбома", replyMarkup: keyboard);
                        }
                        else if (callback != null && callback.Data == CommandHelper.CANCEL_COMMAND)
                        {
                            ResetState();
                            if (callback.Message != null)
                                await client.SendTextMessageAsync(callback.Message.Chat.Id, "Отмена операции");
                        }
                        if (message?.Text != null)
                        {
                            var album = ReplaceText(message.Text, lengthCommand);
                            foreach (var track in trackUpdate)
                            {
                                if (musicDb?.Tracks != null && musicDb.Tracks.Any())
                                {
                                    var t = musicDb.Tracks.FirstOrDefault(x => x.Album == track.Album);
                                    if (t != null)
                                    {
                                        t.Album = album;
                                        musicDb.Tracks.Update(t);
                                        musicDb.SaveChanges();
                                    }
                                }
                            }
                            await client.SendTextMessageAsync(message.Chat.Id, "Название альбома успешно изменено.");
                            trackUpdate.Clear();
                            ResetState();
                        }
                    }
                    break; 
                case State.Qenre:
                    if (tracks != null && tracks.Any())
                    {

                        if (callback != null && callback.Data != CommandHelper.CANCEL_COMMAND)
                        {
                            foreach (var track in tracks)
                                if (track.Qenre == callback.Data)
                                    trackUpdate.Add(track);
                            if (callback.Message != null)
                                await client.SendTextMessageAsync(callback.Message.Chat.Id, "Введите новое название жанра", replyMarkup: keyboard);
                        }
                        else if (callback != null && callback.Data == CommandHelper.CANCEL_COMMAND)
                        {
                            ResetState();
                            if (callback.Message != null)
                                await client.SendTextMessageAsync(callback.Message.Chat.Id, "Отмена операции");
                        }
                        if (message?.Text != null)
                        {
                            var qenre = ReplaceText(message.Text, lengthCommand);
                            foreach (var track in trackUpdate)
                            {
                                if (musicDb?.Tracks != null && musicDb.Tracks.Any())
                                {
                                    var t = musicDb.Tracks.FirstOrDefault(x => x.Qenre == track.Qenre);
                                    if (t != null)
                                    {
                                        t.Qenre = qenre;
                                        musicDb.Tracks.Update(t);
                                        musicDb.SaveChanges();
                                    }
                                }
                            }
                            await client.SendTextMessageAsync(message.Chat.Id, "Название жанра успешно изменено.");
                            trackUpdate.Clear();
                            ResetState();
                        }
                    }
                    break;
                case State.Title:
                    if (tracks != null && tracks.Any())
                    {

                        if (callback != null && callback.Data != CommandHelper.CANCEL_COMMAND)
                        {
                            foreach (var track in tracks)
                                if (track.Title == callback.Data)
                                    trackUpdate.Add(track);
                            if (callback.Message != null)
                                await client.SendTextMessageAsync(callback.Message.Chat.Id, "Введите новое название песни", replyMarkup: keyboard);
                        }
                        else if (callback != null && callback.Data == CommandHelper.CANCEL_COMMAND)
                        {
                            ResetState();
                            if (callback.Message != null)
                                await client.SendTextMessageAsync(callback.Message.Chat.Id, "Отмена операции");
                        }
                        if (message?.Text != null)
                        {
                            var title = ReplaceText(message.Text, lengthCommand);
                            foreach (var track in trackUpdate)
                            {
                                if (musicDb?.Tracks != null && musicDb.Tracks.Any())
                                {
                                    var t = musicDb.Tracks.FirstOrDefault(x => x.Title == track.Title);
                                    if (t != null)
                                    {
                                        t.Title = title;
                                        musicDb.Tracks.Update(t);
                                        musicDb.SaveChanges();
                                    }
                                }
                            }
                            await client.SendTextMessageAsync(message.Chat.Id, "Название песни успешно изменено.");
                            trackUpdate.Clear();
                            ResetState();
                        }
                    }
                    break;
            }
        }

        public UpdateCommand()
        {
            Name = CommandHelper.UPDATE_COMMAND;
            values = new List<string>() { State.Author.ToString(), State.Album.ToString(), State.Qenre.ToString(), State.Title.ToString() };
            trackUpdate = new List<Track>();
        }
    }
}
