﻿using Music_Player_Core.DataBase;
using Music_Player_Core.DataBase.Tables;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace Music_Player_Core.Commands
{
    /// <summary>
    /// Команда "Сортировка".
    /// </summary>
    public class SortCommand : BaseCommand
    {
        #region Поля и свойства

        public override string Name { get; }

        /// <summary>
        /// Список кнопок для инлайн клавиатуры.
        /// </summary>
        private readonly List<string> values;

        /// <summary>
        /// Список из списков песен по 10 штук.
        /// </summary>
        private List<List<Track>> trackList;

        /// <summary>
        /// Общее количество листов.
        /// </summary>
        private int amountList;

        /// <summary>
        /// Текущий лист.
        /// </summary>
        private int currentList = 1;

        /// <summary>
        /// ID сообщения, которое необходимо редактировать.
        /// </summary>
        private int messageId;

        #endregion

        #region Override methods

        public override async Task Execute(ITelegramBotClient client, Update update, MusicDbContext musicDb)
        {
            var message = update.Message;
            var callback = update.CallbackQuery;
            var tracks = musicDb?.Tracks?.ToList();

            if (tracks != null)
            {
                if (State == State.Empty)
                {
                    if (message != null)
                        await SendMessage(values, "Выберите вариант сортировки:", client, message.Chat.Id);
                    return;
                }
                else if (State == State.Wait)
                {
                    if (callback?.Data != null)
                    {
                        if (callback.Data.Contains(values[0]))
                        {
                            var data = tracks.OrderBy(x => x.Title).OrderBy(x => x.Album).OrderBy(x => x.Author).ToList();
                            if (data.Count > 10)
                            {
                                ViewTracksIfMoreTen(client, callback, data);
                            }
                            else
                            {
                                if (callback?.Message != null)
                                    await client.SendTextMessageAsync(callback.Message.Chat.Id, string.Join("\n", data));
                                ResetState();
                            }
                            return;
                        }
                        /*else if (callbackQuery.Data.Contains(values[1]))
                        {
                            var data = tracks.OrderBy(x => x.Title).OrderBy(x => x.Author).OrderBy(x => x.Album).ToList();
                            await client.SendTextMessageAsync(callbackQuery.Message.Chat.Id, string.Join("\n", data));
                            State.ResetStatus();
                            return;
                        }
                        else if (callbackQuery.Data.Contains(values[2]))
                        {
                            var data = tracks.OrderBy(x => x.Title).OrderBy(x => x.Author).OrderBy(x => x.Qenre).ToList();
                            await client.SendTextMessageAsync(callbackQuery.Message.Chat.Id, string.Join("\n", data));
                            State.ResetStatus();
                            return;
                        }*/
                        else if (callback.Data.Contains(values[1]))
                        {
                            var data = tracks.OrderBy(x => x.Album).OrderBy(x => x.Author).OrderBy(x => x.Title).ToList();
                            if (data.Count > 10)
                            {
                                ViewTracksIfMoreTen(client, callback, data);
                            }
                            else
                            {
                                if (callback?.Message != null)
                                    await client.SendTextMessageAsync(callback.Message.Chat.Id, string.Join("\n", data));
                                ResetState();
                            }
                            return;
                        }
                        else if (callback.Data == "Next")
                        {
                            if (currentList < amountList)
                            {
                                currentList++;
                                if (callback?.Message != null)
                                    await client.EditMessageTextAsync(callback.Message.Chat.Id, messageId, string.Join("\n", trackList[currentList - 1]), replyMarkup: InlineKeyboardBackNext(currentList, amountList));
                                return;
                            }
                        }
                        else if (callback.Data == "Back")
                        {
                            if (currentList > 1)
                            {
                                currentList--;
                                if (callback?.Message != null)
                                    await client.EditMessageTextAsync(callback.Message.Chat.Id, messageId, string.Join("\n", trackList[currentList - 1]), replyMarkup: InlineKeyboardBackNext(currentList, amountList));
                                return;
                            }
                        }
                        else if (callback.Data == "Current")
                        {
                            return;
                        }
                        else
                        {
                            ResetState();
                            if (callback?.Message != null)
                                await client.SendTextMessageAsync(callback.Message.Chat.Id, "Произошла ошибка. Попробуйте снова.");
                            return;
                        }
                    }
                    else
                    {
                        ResetState();
                        if (message != null)
                            await client.SendTextMessageAsync(message.Chat.Id, "Произошла ошибка. Попробуйте снова.");
                        return;
                    }
                }
            }
        }

        #endregion

        #region Методы

        /// <summary>
        /// Вывод списка песен, если их количество больше 10 штук.
        /// </summary>
        /// <param name="client">Клиет</param>
        /// <param name="callback">Каллбэк</param>
        /// <param name="data">Список песен</param>
        private void ViewTracksIfMoreTen(ITelegramBotClient client, CallbackQuery callback, List<Track> data)
        {
            amountList = (int)Math.Ceiling((decimal)data.Count / 10);
            trackList = TrackList(data);
            if (callback?.Message != null)
            {
                var result = client.SendTextMessageAsync(callback.Message.Chat.Id, string.Join("\n", trackList[currentList - 1]), replyMarkup: InlineKeyboardBackNext(currentList, amountList));
                messageId = result.Result.MessageId;
            }
        }

        #endregion

        #region Конструктор

        /// <summary>
        /// Конструктор класса.
        /// </summary>
        public SortCommand() : base()
        {
            Name = CommandHelper.SORTING_COMMAND;
            values = new List<string> { State.Author.ToString(), /*Status.Album.ToString(), Status.Qenre.ToString(),*/ State.Title.ToString() };
            trackList = new List<List<Track>>();
        }

        #endregion
    }
}
