﻿using Music_Player_Core.DataBase.Tables;
using Telegram.Bot.Types.ReplyMarkups;
using Telegram.Bot;
using Telegram.Bot.Types;
using Music_Player_Core.DataBase;

namespace Music_Player_Core.Commands
{
    /// <summary>
    /// Базовый класс команда.
    /// </summary>
    public abstract class BaseCommand
    {
        #region Поля и свойства

        /// <summary>
        /// Название команды
        /// </summary>
        public abstract string Name { get; }

        /// <summary>
        /// Статус команды
        /// </summary>
        public State State { get; set; }

        #endregion

        #region Методы

        /// <summary>
        /// Выполнение команды
        /// </summary>
        /// <param name="client">Клиент</param>
        /// <param name="update">Обновления от клиента</param>
        /// <param name="tracks">Список песен</param>
        /// <returns>Задача</returns>
        public abstract Task Execute(ITelegramBotClient client, Update update, MusicDbContext musicDbContext);

        /// <summary>
        /// Проверка на совпадение сообщения с названием команды.
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <returns>True - если имя команды и сообщение совпадают, иначе false.</returns>
        public bool ContainsCommand(string message) => message.Contains(Name);

        /// <summary>
        /// Проверка статуса команды.
        /// </summary>
        /// <returns>Если статус команды не пустой, тогда true, иначе false.</returns>
        public bool ContainsStateNotEmpty()
        {
            return State != State.Empty;
        }

        /// <summary>
        /// Отправка сообщения со встроенной клавиатурой.
        /// </summary>
        /// <param name="values">Список для встроенной клавиатуры</param>
        /// <param name="state">Объект состояния.</param>
        /// <param name="text">Текст сообщения</param>
        /// <param name="botClient">Клиент.</param>
        /// <param name="id">Ид пользователя</param>
        protected async Task SendMessage(List<string>? values, string text, ITelegramBotClient botClient, long id)
        {

            var keyboard = CreateInlineKeyboards(values);
            await botClient.SendTextMessageAsync(id, text, replyMarkup: keyboard);
            State = State.Wait;
        }

        /// <summary>
        /// Создание встроенной клавиатуры.
        /// </summary>
        /// <param name="values">Список значений для клавиатуры.</param>
        /// <returns>Объект клавиатуры.</returns>
        protected InlineKeyboardMarkup CreateInlineKeyboards(List<string>? values)
        {
            var buttons = new List<List<InlineKeyboardButton>>();
            var count = 0;
            var list = new List<InlineKeyboardButton>();

            if (values != null)
            {
                for (int i = 0; i < values.Count; i++)
                {
                    if (count < 2 && values.Count - i != 1)
                    {
                        list.Add(new InlineKeyboardButton(values[i]) {  CallbackData = values[i] });
                        count++;
                    }
                    else if (count == 2)
                    {
                        buttons.Add(list);
                        list = new List<InlineKeyboardButton>();
                        count = 0;
                        i--;
                    }
                    else
                    {
                        list.Add(new InlineKeyboardButton(values[i]) { CallbackData = values[i] });
                        buttons.Add(list);
                    }
                }
            }
            return new InlineKeyboardMarkup(buttons);
        }

        /// <summary>
        /// Создание списка песен, если их больше 10.
        /// </summary>
        /// <param name="tracks">Исходный список песен.</param>
        /// <returns>Список списков песен по 10 штук.</returns>
        protected List<List<Track>> TrackList(List<Track> tracks)
        {
            var result = new List<List<Track>>();
            if (tracks.Count < 11)
            {
                result.Add(tracks);
                return result;
            }
            else
            {
                var start = 0;
                var end = 10;
                var amountList = (int)Math.Ceiling((decimal)tracks.Count / 10);
                for (int i = 0; i < amountList; i++)
                {
                    var newList = new List<Track>();
                    for (int j = start; j < end; j++)
                    {
                        newList.Add(tracks[j]);
                    }
                    result.Add(newList);
                    start += 10;
                    end += 10;
                    if (end > tracks.Count) end = tracks.Count;
                }
                return result;
            }
        }

        /// <summary>
        /// Создание инлайн клавиатуры для переключения списков всех песен.
        /// </summary>
        /// <param name="currentList">Текущий лист</param>
        /// <param name="amountList">Всего листов</param>
        /// <returns>Инлайн клавиатура.</returns>
        protected InlineKeyboardMarkup InlineKeyboardBackNext(int currentList, int amountList)
        {
            var buttons = new List<InlineKeyboardButton>
                        {
                        new InlineKeyboardButton("⬅️"){ CallbackData = "Back"},
                        new InlineKeyboardButton($@"{currentList}/{amountList}"){ CallbackData = "Current"},
                        new InlineKeyboardButton("➡️"){ CallbackData = "Next"}
                        };
            return new InlineKeyboardMarkup(buttons);
        }

        /// <summary>
        /// Обнуление статуса.
        /// </summary>
        protected void ResetState()
        {
            State = State.Empty;
        }

        /// <summary>
        /// Создание выбора для удаления.
        /// </summary>
        /// <param name="client">Клиент</param>
        /// <param name="callback">Коллбэк</param>
        /// <param name="values">Список вариантов</param>
        /// <param name="text">Текст сообщения</param>
        /// <param name="state">Обновленный статус</param>
        /// <returns>Задача.</returns>
        protected async Task Selection(ITelegramBotClient client, CallbackQuery callback, List<string>? values, string text, State state)
        {
            if (callback.Message != null && values != null)
            {
                values.Add(CommandHelper.CANCEL_COMMAND);
                var keyboard = CreateInlineKeyboards(values);
                await client.SendTextMessageAsync(callback.Message.Chat.Id, text, replyMarkup: keyboard);
                State = state;
            }
            return;
        }

        /// <summary>
        /// Замена текста сообщения.
        /// </summary>
        /// <param name="text">Текст сообщения.</param>
        /// <returns>Измененный текст.</returns>
        protected string ReplaceText(string text, int length)
        {
            return text.Substring(length + 1);
        }

        #endregion

        #region Конструктор

        /// <summary>
        /// Конструтор класса.
        /// </summary>
        public BaseCommand()
        {
            State = State.Empty;
        }

        #endregion
    }
}