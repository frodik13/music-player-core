﻿using Music_Player_Core.DataBase.Tables;
using Music_Player_Core.DataBase;
using Telegram.Bot.Types;
using Telegram.Bot;
using File = System.IO.File;

namespace Music_Player_Core.Commands
{
    /// <summary>
    /// Команда "Удаление".
    /// </summary>
    public class DeleteCommand : BaseCommand
    {
        #region Поля и свойства

        public override string Name { get; }

        /// <summary>
        /// Список кнопок для инлайн клавиатуры.
        /// </summary>
        private readonly List<string> valuesInlineKeyboards;

        #endregion

        #region Override methods

        public override async Task Execute(ITelegramBotClient client, Update update, MusicDbContext musicDb)
        {
            if (update != null)
            {
                var message = update.Message;
                var callback = update.CallbackQuery;
                var tracks = musicDb?.Tracks?.ToList();


                if (tracks == null || !tracks.Any())
                {
                    if (callback?.Message != null)
                    {
                        await client.SendTextMessageAsync(callback.Message.Chat.Id, "Ошибка. Песни отсутствуют.");
                        return;
                    }
                }

                switch (State)
                {
                    case State.Empty:
                        if(message != null) 
                            await SendMessage(valuesInlineKeyboards, "По какому параметру выполнить удаление?", client, message.Chat.Id);
                        break;
                    case State.Wait:
                        if (callback?.Data != null)
                        {
                            if (tracks != null && tracks.Any())
                            {
                                if (callback.Data.Contains(valuesInlineKeyboards[0]))
                                {
                                    var valuesAuthor = tracks.Select(x => x.Author).Distinct().ToList();
                                    await Selection(client, callback, valuesAuthor, "Выберите автора, которого хотите удалить:", State.Author);
                                }
                                else if (callback.Data.Contains(valuesInlineKeyboards[1]))
                                {
                                    var valuesAlbum = tracks.Select(x => x.Album).Distinct().ToList();
                                    await Selection(client, callback, valuesAlbum, "Выберите альбом который хотите удалить:", State.Album);

                                }
                                else if (callback.Data.Contains(valuesInlineKeyboards[2]))
                                {
                                    var valuesQenre = tracks.Select(x => x.Qenre).Distinct().ToList();
                                    await Selection(client, callback, valuesQenre, "Выберите жанр который хотите удалить:", State.Qenre);

                                }
                                else if (callback.Data.Contains(valuesInlineKeyboards[3]))
                                {
                                    var valuesTitle = tracks.Select(x => x.Title).ToList();
                                    await Selection(client, callback, valuesTitle, "Выберите трек который хотите удалить:", State.Title);

                                }
                                else if (callback.Data.Contains(CommandHelper.CANCEL_COMMAND))
                                {
                                    ResetState();
                                    if (callback.Message != null)
                                        await client.SendTextMessageAsync(callback.Message.Chat.Id, "Операция отменена.");
                                }
                            }
                        }
                        break;
                    case State.Author:
                        if (callback != null)
                        {
                            if (musicDb?.Tracks != null)
                            {
                                foreach (var track in musicDb.Tracks)
                                {
                                    if (track.Author == callback.Data)
                                    {
                                        await RemoveFromDataBase(client, musicDb, callback, track);
                                    }
                                }
                            }
                        }
                        break;
                    case State.Title:
                        if (callback != null)
                        {
                            if (musicDb?.Tracks != null)
                            {
                                foreach (var track in musicDb.Tracks)
                                {
                                    if (track.Title == callback.Data)
                                    {
                                        await RemoveFromDataBase(client, musicDb, callback, track);
                                    }
                                }
                            }
                        }
                        break;
                    case State.Qenre:
                        if (callback != null)
                        {
                            if (musicDb?.Tracks != null)
                            {
                                foreach (var track in musicDb.Tracks)
                                {
                                    if (track.Qenre == callback.Data)
                                    {
                                        await RemoveFromDataBase(client, musicDb, callback, track);
                                    }
                                }
                            }
                        }
                        break;
                    case State.Album:
                        if (callback != null)
                        {
                            if (musicDb?.Tracks != null)
                            {
                                foreach (var track in musicDb.Tracks)
                                {
                                    if (track.Album == callback.Data)
                                    {
                                        await RemoveFromDataBase(client, musicDb, callback, track);
                                    }
                                }
                            }
                        }
                        break;
                }
            }
        }



        #endregion

        #region Методы

        private static async Task RemoveFromDataBase(ITelegramBotClient client, MusicDbContext musicDb, CallbackQuery? callback, Track track)
        {
            if (callback != null)
            {
                var source = track.Source;
                if (source != null)
                {
                    var path = TelegramUI.directory + source;
                    if (File.Exists(path))
                    {
                        File.Delete(path);
                        if (callback.Message != null)
                            await client.SendTextMessageAsync(callback.Message.Chat.Id, $"Файл с названием {source} удален из ресурсов.");
                    }
                }
                var remove = musicDb.Tracks?.Remove(track);
                musicDb.SaveChanges();
                if(callback.Message != null && remove?.State == Microsoft.EntityFrameworkCore.EntityState.Deleted)
                    await client.SendTextMessageAsync(callback.Message.Chat.Id, $"Файл с названием {source} удален из БД.");
            }
        }

        #endregion

        #region Конструктор

        /// <summary>
        /// Конструктор класса.
        /// </summary>
        public DeleteCommand() : base()
        {
            Name = CommandHelper.DELETE_COMMAND;
            valuesInlineKeyboards = new List<string> { State.Author.ToString(), State.Album.ToString(), State.Qenre.ToString(), State.Title.ToString(), CommandHelper.CANCEL_COMMAND };
        }

        #endregion
    }
}
