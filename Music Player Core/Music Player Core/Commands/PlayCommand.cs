﻿using Music_Player_Core.DataBase;
using Music_Player_Core.DataBase.Tables;
using Telegram.Bot;
using Telegram.Bot.Types;
using File = System.IO.File;

namespace Music_Player_Core.Commands
{
    /// <summary>
    /// Класс "Команда воспроизведения"
    /// </summary>
    public class PlayCommand : BaseCommand
    {
        #region Поля и свойства

        public override string Name { get; }

        /// <summary>
        /// Значение поиска по автору.
        /// </summary>
        private string? author;

        #endregion

        #region Override methods

        public override async Task Execute(ITelegramBotClient client, Update update, MusicDbContext musicDb)
        {
            var message = update.Message;
            var callback = update.CallbackQuery;
            var tracks = musicDb?.Tracks?.ToList();

            if (tracks == null || !tracks.Any())
            {
                if(message != null)
                    await client.SendTextMessageAsync(message.Chat.Id, "Треков еще нету :(");
                return;
            }

            switch (State)
            {
                case State.Empty:
                    var authors = tracks.Select(x => x.Author).Distinct().ToList();
                    var keyboardMarkupAuthor = CreateInlineKeyboards(authors);
                    if(message != null)
                        await client.SendTextMessageAsync(message.Chat.Id, "Выберите автора: ", replyMarkup: keyboardMarkupAuthor);
                    State = State.Author;
                    break;
                case State.Author:
                    if (callback?.Data != null)
                    {
                        var tracksList = tracks.Where(x => x.Author == callback.Data).Select(x => x.Title).ToList();
                        author = callback.Data;
                        var keyboardMarkupTracks = CreateInlineKeyboards(tracksList);
                        if(callback.Message != null)
                            await client.SendTextMessageAsync(callback.Message.Chat.Id, "Выберите трек: ", replyMarkup: keyboardMarkupTracks);
                        State = State.Title;
                    }
                    break;
                case State.Title:
                    if (callback?.Data != null)
                    {
                        if (tracks.Select(x => x.Title).ToList().Contains(callback.Data))
                        {
                            var track = tracks.Where(x => x.Title == callback.Data && x.Author == author)
                                                .Select(x => x.Source)
                                                .First();
                            if (callback.Message != null)
                            {
                                try
                                {
                                    using var stream = File.OpenRead(TelegramUI.directory + track);
                                    if(stream != null)
#pragma warning disable CS8604 // Возможно, аргумент-ссылка, допускающий значение NULL.
                                        await client.SendAudioAsync(callback.Message.Chat.Id, stream, performer: author, title: callback.Data);
#pragma warning restore CS8604 // Возможно, аргумент-ссылка, допускающий значение NULL.
                                }
                                catch
                                {
                                    LogHelper.Error($"Не найдена песня в базе данных {callback.Data}");
                                    await client.SendTextMessageAsync(callback.Message.Chat.Id, "Песня не найдена.");
                                }
                            }
                        }
                        else
                        {
                            if (callback.Message != null)
                                await client.SendTextMessageAsync(callback.Message.Chat.Id, "Произошла ошибка. Попроуйте снова.");
                        }
                    }
                    ResetState();
                    break;
            }
        }

        #endregion

        #region Конструктор

        /// <summary>
        /// Конструктор класса.
        /// </summary>
        public PlayCommand() : base()
        {
            Name = CommandHelper.PLAY_COMMAND;
        }

        #endregion
    }
}
