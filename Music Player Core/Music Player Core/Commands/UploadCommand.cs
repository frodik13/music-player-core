﻿using Music_Player_Core.DataBase.Tables;
using Music_Player_Core.DataBase;
using Telegram.Bot.Types.ReplyMarkups;
using Telegram.Bot.Types;
using Telegram.Bot;
using File = System.IO.File;

namespace Music_Player_Core.Commands
{
    /// <summary>
    /// Команда "Загрузка".
    /// </summary>
    public class UploadCommand : BaseCommand
    {
        #region Поля и свойства

        public override string Name { get; }

        /// <summary>
        /// Длина текста команды загрузка.
        /// </summary>
        private readonly int lengthCommand = CommandHelper.UPDATE_COMMAND.Length;

        /// <summary>
        /// Автор песни.
        /// </summary>
        private string? author;

        /// <summary>
        /// Название песни
        /// </summary>
        private string? title;

        /// <summary>
        /// Альбом песни.
        /// </summary>
        private string? album;

        /// <summary>
        /// Жанр песни.
        /// </summary>
        private string? qenre;

        /// <summary>
        /// Адрес файла песни.
        /// </summary>
        private string? source;

        /// <summary>
        /// Строка для команды отменить.
        /// </summary>
        private const string CANCEL = "Отменить";

        #endregion

        #region Override methods

        public async override Task Execute(ITelegramBotClient client, Update update, MusicDbContext musicDb)
        {
            var message = update.Message;
            var callback = update.CallbackQuery;
            var keyboard = new InlineKeyboardMarkup(new InlineKeyboardButton(CANCEL) { CallbackData = CANCEL });

            switch (State)
            {
                case State.Empty:
                    if(message != null)
                        await client.SendTextMessageAsync(message.Chat.Id, "Отправьте имя исполнителя:", replyMarkup: keyboard);
                    State = State.Author;
                    break;
                case State.Author:
                    if (callback != null)
                    {
                        await CancelUpload(client, callback);
                    }
                    else
                    {
                        if (message?.Text != null)
                        {
                            author = ReplaceText(message.Text, lengthCommand);
                            await client.SendTextMessageAsync(message.Chat.Id, "Введите название трека:", replyMarkup: keyboard);
                            State = State.Title;
                        }
                    }
                    break;
                case State.Title:
                    if (callback != null)
                    {
                        await CancelUpload(client, callback);
                    }
                    else
                    {
                        if (message?.Text != null)
                        {
                            title = ReplaceText(message.Text, lengthCommand);
                            await client.SendTextMessageAsync(message.Chat.Id, "Введите название альбома. Если не знаете, напишите 'empty'", replyMarkup: keyboard);
                            State = State.Album;
                        }
                    }
                    break;
                case State.Album:
                    if (callback != null)
                    {
                        await CancelUpload(client, callback);
                    }
                    else
                    {
                        if (message?.Text != null)
                        {
                            album = ReplaceText(message.Text, lengthCommand);
                            await client.SendTextMessageAsync(message.Chat.Id, "Введите название жанра. Если не знаете, напишите 'empty'", replyMarkup: keyboard);
                            State = State.Qenre;
                        }
                    }
                    break;
                case State.Qenre:
                    if (callback != null)
                    {
                        await CancelUpload(client, callback);
                    }
                    else
                    {
                        if (message?.Text != null)
                        {
                            qenre = ReplaceText(message.Text, lengthCommand);
                            await client.SendTextMessageAsync(message.Chat.Id, "Отправьте трек", replyMarkup: keyboard);
                            State = State.Source;
                        }
                    }
                    break;
                case State.Source:
                    if (callback != null)
                    {
                        await CancelUpload(client, callback);
                    }
                    else
                    {
                        if (message?.Audio != null)
                        {
                            var fileId = message.Audio.FileId;
                            var fileInfo = await client.GetFileAsync(fileId);
                            var filePath = fileInfo.FilePath;
                            if (message.Audio.FileName != null)
                            {
                                var fileName = message.Audio.FileName;
                                var destinationPath = Path.Combine(@"\Resources", message.Audio.FileName);
                                using (var fs = File.OpenWrite(TelegramUI.directory + destinationPath))
                                {
                                    if(filePath != null)
                                        await client.DownloadFileAsync(filePath, fs);
                                }

                                source = destinationPath;
                                var track = new Track(title, author, album, qenre, source);

                                musicDb.Add(track);
                                musicDb.SaveChanges();

                                await client.SendTextMessageAsync(message.Chat.Id, $"Песня под названием {fileName} успешно добавлена в базу");
                            }

                            ResetState();
                            RemoveValues();
                        }
                        else
                        {
                            if (message != null)
                                await client.SendTextMessageAsync(message.Chat.Id, "Необходимо отправить файл с песней.", replyMarkup: keyboard);
                        }
                    }
                    break;
            }
        }

        #endregion

        #region Методы

        /// <summary>
        /// Отмена загрузки.
        /// </summary>
        /// <param name="client">Клиент</param>
        /// <param name="callback">Каллбэк</param>
        /// <returns></returns>
        private async Task CancelUpload(ITelegramBotClient client, CallbackQuery callback)
        {
            ResetState();
            RemoveValues();
            if(callback?.Message != null)
                await client.SendTextMessageAsync(callback.Message.Chat.Id, "Отмена добавления");
            return;
        }
        
        /// <summary>
        /// Очистить все значения.
        /// </summary>
        private void RemoveValues()
        {
            title = "";
            author = "";
            album = "";
            qenre = "";
            source = "";
        }

        #endregion

        #region Конструктор

        /// <summary>
        /// Конструктор класса.
        /// </summary>
        public UploadCommand() : base()
        {
            Name = CommandHelper.UPLOAD_COMMAND;
        }

        #endregion
    }
}
