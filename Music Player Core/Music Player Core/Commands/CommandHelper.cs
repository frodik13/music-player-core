﻿namespace Music_Player_Core.Commands
{
    /// <summary>
    /// Статический класс с константами команд.
    /// </summary>
    public static class CommandHelper
    {
        /// <summary>
        /// Название команды "вывод всех песен".
        /// </summary>
        public const string ALL_TRACK_COMMAND = "all_track";

        /// <summary>
        /// Название команды "прослушать песню или альбом".
        /// </summary>
        public const string PLAY_COMMAND = "play_track";

        /// <summary>
        /// Название команды "загрузить песню".
        /// </summary>
        public const string UPLOAD_COMMAND = "upload";

        /// <summary>
        /// Название команды "изменить".
        /// </summary>
        public const string UPDATE_COMMAND = "update";

        /// <summary>
        /// Название команды "удалить".
        /// </summary>
        public const string DELETE_COMMAND = "delete";

        /// <summary>
        /// Название команды "сканирование".
        /// </summary>
        public const string SCANNING_COMMAND = "scanning";

        /// <summary>
        /// Название команды "сортировка".
        /// </summary>
        public const string SORTING_COMMAND = "sorting";

        /// <summary>
        /// Название команды "старт".
        /// </summary>
        public const string START_COMMAND = "start";

        /// <summary>
        /// Название команды "отмена".
        /// </summary>
        public const string CANCEL_COMMAND = "Cancel";

        /// <summary>
        /// ID администратора.
        /// </summary>
        public const long ID = 389037282L;
    }
}
