﻿using Music_Player_Core.DataBase.Tables;
using Telegram.Bot.Types;
using Telegram.Bot;
using Music_Player_Core.DataBase;

namespace Music_Player_Core.Commands
{
    /// <summary>
    /// Команда "Старт".
    /// </summary>
    public class StartCommand : BaseCommand
    {
        #region Поля и свойства

        public override string Name { get; }

        #endregion

        #region Override methods

        public async override Task Execute(ITelegramBotClient client, Update update, MusicDbContext musicDb)
        {
            var message = update.Message;

            if (message != null)
            {
                await client.SendTextMessageAsync(message.Chat.Id, "Добро пожаловать в музыкальный бот. Выбери команду из списка команд.");
                var answer = $"/{CommandHelper.ALL_TRACK_COMMAND} - Вывести список всех песен\n" +
                                $"/{CommandHelper.SORTING_COMMAND} - Сортировка\n" +
                                $"/{CommandHelper.SCANNING_COMMAND} - Сканирование\n" +
                                $"/{CommandHelper.PLAY_COMMAND} - Воспроизвести песню";
                await client.SendTextMessageAsync(message.Chat.Id, answer);

                await CreateCommand(client);
            }
            return;
        }

        #endregion

        #region Методы

        /// <summary>
        /// Создание команд для бота.
        /// </summary>
        /// <param name="id">Ид пользователя.</param>
        /// <param name="client">Клиент.</param>
        /// <returns></returns>
        private async Task CreateCommand(long id, ITelegramBotClient client)
        {
            var botCommandScopeAll = new BotCommandScopeAllChatAdministrators();
            var botCommandScopeDefault = new BotCommandScopeDefault();

            List<BotCommand> commands = new List<BotCommand>()
            {
                new BotCommand() { Command = CommandHelper.ALL_TRACK_COMMAND, Description = "Список всех песен" },
                new BotCommand() { Command = CommandHelper.SCANNING_COMMAND, Description = "Сканирование" },
                new BotCommand() { Command = CommandHelper.SORTING_COMMAND, Description = "Сортировка" },
                new BotCommand() { Command = CommandHelper.PLAY_COMMAND, Description = "Воспроизвести песню" }
            };
            if (id != CommandHelper.ID)
            {
                await client.SetMyCommandsAsync(commands, botCommandScopeDefault);
            }
            else
            {
                commands.Add(new BotCommand() { Command = CommandHelper.UPLOAD_COMMAND, Description = "Загрузить песню" });
                commands.Add(new BotCommand() { Command = CommandHelper.UPDATE_COMMAND, Description = "Изменить" });
                commands.Add(new BotCommand() { Command = CommandHelper.DELETE_COMMAND, Description = "Удалить песню" });
                await client.SetMyCommandsAsync(commands, botCommandScopeAll);
            }
        }

        /// <summary>
        /// Создание команд для бота.
        /// </summary>
        /// <param name="id">Ид пользователя.</param>
        /// <param name="client">Клиент.</param>
        /// <returns></returns>
        private async Task CreateCommand(ITelegramBotClient client)
        {

            List<BotCommand> commands = new List<BotCommand>()
            {
                new BotCommand() { Command = CommandHelper.ALL_TRACK_COMMAND, Description = "Список всех песен" },
                new BotCommand() { Command = CommandHelper.SCANNING_COMMAND, Description = "Сканирование" },
                new BotCommand() { Command = CommandHelper.SORTING_COMMAND, Description = "Сортировка" },
                new BotCommand() { Command = CommandHelper.PLAY_COMMAND, Description = "Воспроизвести песню" },
                new BotCommand() { Command = CommandHelper.UPLOAD_COMMAND, Description = "Загрузить песню" },
                new BotCommand() { Command = CommandHelper.UPDATE_COMMAND, Description = "Изменить" },
                new BotCommand() { Command = CommandHelper.DELETE_COMMAND, Description = "Удалить песню" }
            };
            await client.SetMyCommandsAsync(commands);
        }

        #endregion

        #region Конструктор

        /// <summary>
        /// Конструктор класса.
        /// </summary>
        public StartCommand() : base()
        {
            Name = CommandHelper.START_COMMAND;
        }

        #endregion
    }
}
