﻿using Music_Player_Core.DataBase;
using Music_Player_Core.DataBase.Tables;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Music_Player_Core.Commands
{
    /// <summary>
    /// Команда "Сканирование".
    /// </summary>
    public class ScanCommand : BaseCommand
    {
        #region Поля и свойства

        public override string Name { get; }

        /// <summary>
        /// Список кнопок для инлайн клавиатуры.
        /// </summary>
        private readonly List<string> values;

        #endregion

        #region Override methods

        public override async Task Execute(ITelegramBotClient client, Update update, MusicDbContext musicDb)
        {
            var message = update.Message;
            var callback = update.CallbackQuery;
            var tracks = musicDb?.Tracks?.ToList();

            if (tracks != null)
            {
                switch (State)
                {
                    case State.Empty:
                        if (message != null)
                            await SendMessage(values, "По какому параметру хотите провести сканирование?", client, message.Chat.Id);
                        break;
                    case State.Wait:
                        if (callback?.Data != null)
                        {
                            if (callback.Data.Contains(State.Author.ToString()))
                            {
                                var valuesAuthor = tracks.Select(x => x.Author).Distinct().ToList();
                                await ScanStatusAnswer(valuesAuthor, callback, client, "Список авторов");
                                return;
                            }
                            else if (callback.Data.Contains(State.Album.ToString()))
                            {
                                var valuesAlbum = tracks.Select(x => x.Album).Distinct().ToList();
                                await ScanStatusAnswer(valuesAlbum, callback, client, "Список альбомов");
                                return;
                            }
                            else if (callback.Data.Contains(State.Qenre.ToString()))
                            {
                                var valuesQenre = tracks.Select(x => x.Qenre).Distinct().ToList();
                                await ScanStatusAnswer(valuesQenre, callback, client, "Список жанров");
                                return;
                            }
                            else if (callback.Data.Contains(State.Title.ToString()))
                            {
                                var valuesTitle = tracks.Select(x => x.Title).ToList();
                                await ScanStatusAnswer(valuesTitle, callback, client, "Список песен");
                                return;
                            }
                        }
                        break;
                }
            }
        }

        #endregion

        #region Методы

        /// <summary>
        /// Ответ сканирования.
        /// </summary>
        /// <param name="values">Список ответов.</param>
        /// <param name="callback">Сообщение.</param>
        /// <param name="botClient">Клиент.</param>
        /// <param name="text">Текст запрашиваемого запроса.</param>
        private async Task ScanStatusAnswer(List<string> values, CallbackQuery callback, ITelegramBotClient botClient, string text)
        {
            var result = $"*{text}*:\n\n{string.Join("\n", values)}";
            if(callback?.Message != null)
                await botClient.SendTextMessageAsync(callback.Message.Chat.Id, result, parseMode: ParseMode.Markdown);
            ResetState();
        }

        #endregion

        #region Конструктор

        /// <summary>
        /// Конструктор класса.
        /// </summary>
        public ScanCommand() : base()
        {
            Name = CommandHelper.SCANNING_COMMAND;
            values = new List<string> { State.Author.ToString(), State.Album.ToString(), State.Qenre.ToString(), State.Title.ToString() };
        }

        #endregion
    }
}
