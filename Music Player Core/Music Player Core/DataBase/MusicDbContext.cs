﻿using Microsoft.EntityFrameworkCore;
using Music_Player_Core.DataBase.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Music_Player_Core.DataBase
{
    public class MusicDbContext : DbContext
    {

        public DbSet<Track>? Tracks { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=MusicDataBase.sqlite");
        }
    }
}
