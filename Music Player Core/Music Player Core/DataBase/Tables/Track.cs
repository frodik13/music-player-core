﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Music_Player_Core.DataBase.Tables
{
    /// <summary>
    /// Класс "Песня".
    /// </summary>
    public class Track
    {
        #region Поля и свойства

        /// <summary>
        /// ИД.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название песни.
        /// </summary>
        public string Title
        {
            get { return title ?? "Empty title"; }
            set => title = value ?? "Empty title";
        }

        private string? title;

        /// <summary>
        /// Автор песни.
        /// </summary>
        public string Author
        {
            get { return author ?? "Empty Author"; }
            set => author = value ?? "Empty Author";
        }

        private string? author;

        /// <summary>
        /// Альбом.
        /// </summary>
        public string Album
        {
            get { return album ?? "Empty album"; }
            set => album = value ?? "Empty Album";
        }

        private string? album;

        /// <summary>
        /// Жанр.
        /// </summary>
        public string Qenre
        {
            get { return qenre ?? "Empty qenre"; }
            set => qenre = value ?? "Empty Qenre";
        }

        private string? qenre;

        /// <summary>
        /// Ссылка на источник.
        /// </summary>
        public string Source
        {
            get { return source ?? "Empty source"; }
            set => source = value ?? "Empty Source";
        }

        private string? source;

        #endregion

        #region Override methods

        public override string ToString()
        {
            return $"🎸 {Author} - {Title}.\n";
        }

        public override int GetHashCode()
        {
            int hashCode = 674324649;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Title);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Author);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Album);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Qenre);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Source);
            return hashCode;
        }

        public override bool Equals(object? obj)
        {
            return obj is Track track &&
                   Id == track.Id &&
                   Title == track.Title &&
                   Author == track.Author &&
                   Album == track.Album &&
                   Qenre == track.Qenre &&
                   Source == track.Source;
        }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор класса.
        /// </summary>
        /// <param name="title">Название песни.</param>
        /// <param name="author">Автор.</param>
        /// <param name="album">Альбом.</param>
        /// <param name="qenre">Жанр.</param>
        /// <param name="source">Источник на песню.</param>
        public Track(string? title, string? author, string? album, string? qenre, string? source)
        {
            Title = string.IsNullOrEmpty(title) ? "Empty Title" : title;
            Author = string.IsNullOrEmpty(author) ? "Empty Author" : author;
            Album = string.IsNullOrEmpty(album) ? "Empty Album" : album;
            Qenre = string.IsNullOrEmpty(qenre) ? "Empty Qenre" : qenre;
            Source = string.IsNullOrEmpty(source) ? "Empty Source" : source;
        }

        /// <summary>
        /// Конструктор без параметров.
        /// </summary>
        public Track()
        {
        }

        #endregion
    }
}
